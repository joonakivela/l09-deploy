export default class Tax {
    /**
     * 
     * @param {number} list_price before tax
     * @param {*} tax in percentage
     * @returns {number} after tax (total_price)
     */
    static applyTax(list_price, tax){
        const tax_rate = tax / 100;
        const after_tax = list_price + list_price * tax_rate;
        return after_tax;
    }
    static deductTax(total_price, tax){
        const tax_rate = 1 + (tax /100);
        const list_price = total_price /tax_rate;
        return list_price;
    }
    static figureTaxRate(list_price, total_price){
        const tax_rate = (1 / (list_price / total_price)) - 1;
        const tax_rate_percent = tax_rate * 100;
        return tax_rate_percent;

    }
}